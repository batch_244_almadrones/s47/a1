console.log("hello Feb!");

// for selectingHTML elemetns we will using the document.querySelector()
// syntax - document.querySelector("htmlElement")
// documnet refers to the whole page
// .querySelector - used to seclect a specific object(HTML element) from the document/webpage
// the querySelector takes a string input that is formatted like a CSS selector when applying the styles

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");
let fullName = { fName: "", lName: "" };
txtFirstName.addEventListener("keyup", (event) => {
  fullName.fName = txtFirstName.value;
  spanFullName.innerHTML = `${fullName.fName} ${fullName.lName}`;
});

txtLastName.addEventListener("keyup", (event) => {
  fullName.lName = txtLastName.value;
  spanFullName.innerHTML = `${fullName.fName} ${fullName.lName}`;
});
